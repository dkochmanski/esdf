(in-package #:2am-esdf)

;;; Some syntactic sugar for 2am
(defmacro once-only (specs &body body)
  "Once-Only ({(Var Value-Expression)}*) Form*

  Create a Let* which evaluates each Value-Expression, binding a
  temporary variable to the result, and wrapping the Let* around the
  result of the evaluation of Body.  Within the body, each Var is
  bound to the corresponding temporary variable."
  (labels ((frob (specs body)
             (if (null specs)
                 `(progn ,@body)
                 (let ((spec (first specs)))
                   (when (/= (length spec) 2)
                     (error "Malformed Once-Only binding spec: ~S." spec))
                   (let ((name (first spec))
                         (exp-temp (gensym)))
                     `(let ((,exp-temp ,(second spec))
                            (,name (gensym "OO-")))
                        `(let ((,,name ,,exp-temp))
                           ,,(frob (rest specs) body))))))))
    (frob specs body)))


(defmacro is-true (form)
  (once-only ((result form))
    `(is (eql ,result t) "Expected T, but got ~s" ,result)))

(defmacro is-false (form)
  (once-only ((result form))
    `(is (null ,result) "Expected NIL, but got ~s" ,result)))

(defmacro is-equal (what form)
  (once-only ((what what)
              (form form))
    `(is (equal ,what ,form) "EQUAL: ~s to ~s" ',form ,what ,form)))

(defmacro is-eql (what form)
  (once-only ((what what)
              (form form))
    `(is (eql ,what ,form) "EQL: ~s to ~s" ,what ,form)))

(defmacro pass (form &rest args)
  (declare (ignore form args))
  `(passed))

(defmacro fail (form &rest args
                &aux
                  (fmt-ctrl (or (car args) ""))
                  (fmt-args (cdr args)))
  (declare (ignore form))
  `(failed (make-condition 'test-failure
                           :name *test-name*
                           :format-control ,fmt-ctrl
                           :format-arguments (list ,@fmt-args))))

(export '(is-true is-false is-equal is-eql pass fail) *package*)
