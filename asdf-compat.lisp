(in-package #:esdf)

(eval-when (:load-toplevel :compile-toplevel :execute)
  (when (member :asdf-compat *features*)
    (let ((package (find-package "ASDF")))
      (when (and package
                 (find-symbol "UPGRADE-ASDF" package))
        (rename-package
         "ASDF/INTERFACE" "ASDF/INTERFACE"
         (remove "ASDF" (package-nicknames package)
                 :test #'equalp))))))

(defpackage #:esdf/asdf-compat
  (:nicknames #:asdf)
  (:use #:cl #:esdf #:alexandria #:split-sequence)
  (:import-from #:esdf/system-discovery
                #:ensure-load-hook
                #:*central-registry*
                #:*system-definition-search-functions*)
  (:export
   #| main operations |#
   #:oos #:operate #:perform
   #| exported operations |#
   #:load-op #:load-source-op #:load-bundle-op #:monolithic-load-bundle-op
   #:test-op #:compile-op
   #| functions operating on systems and its definitions |#
   #:system #:defsystem #:load-asd #:system-source-directory
   #:load-system #:require-system #:test-system
   #| ASDF interface stubs |#
   #:find-system #:clear-system #:component-name #:component-pathname
   #:initialize-source-registry
   #:map-systems
   #| conditions |#
   #:missing-requires #:missing-required-by
   #:missing-dependency #:missing-dependency-of-version
   #:missing-component
   #| commonly used functions (simpler equivalents) |#
   #:symbol-call #:version-satisies #:asdf-version
   #| symbols polluted to the interface by the original ASDF |#
   #:*central-registry* #:*system-definition-search-functions* #:*defined-systems*))
(in-package #:asdf)


;;; almighty `defsystem' macro
(defmacro defsystem (name &body options
                     &key (class 'system) &allow-other-keys)
  (labels ((fix-option (new-name old-name
                        &key (options options) (key #'identity) splice
                        &aux (new-name (intern (symbol-name new-name)
                                               (find-package '#:esdf))))
             (when-let ((val (funcall key (getf options old-name))))
               (list (if splice
                         (cons new-name val)
                         (list new-name val)))))
           (process-components (comp)
             (mapcar
              #'(lambda (c)
                  (destructuring-bind
                        (type name &rest options) c
                    `(,type ,name
                      ,@(fix-option 'pathname :pathname :options options)
                      ,@(fix-option 'depends :depends-on :options options
                                    :splice t)
                      ,@(fix-option 'if-feature :if-feature :options options)
                      ,@(fix-option 'components :components :options options
                                    :splice t :key #'process-components))))
              comp))
           (process-dependencies ()
             (let ((weak (mapcar #'(lambda (dep) `(:weakly ,dep))
                                 (getf options :weakly-depends-on))))
               (when-let ((depends (append (getf options :depends-on) weak)))
                 (list (cons 'esdf::depends depends))))))
    `(define-system ,(esdf:coerce-name name) (,class)
       ,@(fix-option 'pathname            :pathname)
       ,@(fix-option 'if-feature          :if-feature)
       ,@(fix-option 'definition-depends  :defsystem-depends-on :splice t)
       ,@(fix-option 'components          :components           :splice t
                     :key #'process-components)
       ,@(process-dependencies)

       ;; meta is for potential package managers, extensions etc.
       (esdf:meta ,@(fix-option 'name :name
                                :options `(:name ,(coerce-name name)))
                  ,@(fix-option 'long-name        :long-name)
                  ,@(fix-option 'description      :description)
                  ,@(fix-option 'long-description :long-description)
                  ,@(fix-option 'author           :author)
                  ,@(fix-option 'maintainer       :maintainer)
                  ,@(fix-option 'licence          :licence)
                  ,@(fix-option 'version          :version)
                  ,@(fix-option 'homepage         :homepage)
                  ,@(fix-option 'bug-tracker      :bug-tracker)
                  ,@(fix-option 'mailto           :mailto)
                  ,@(fix-option 'source-control   :source-control))

       ;; keep all options (maybe extensions will use them)
       (all-args ,options))))


;;; commonly used functions

(defun asdf-version () "3.1")

(defun version-satisfies (version required-version)
  (let ((v1 (split-sequence #\. version))
        (v2 (split-sequence #\. required-version)))
    (map () #'(lambda (v1 v2)
                (let ((v1 (parse-integer v1))
                      (v2 (parse-integer v2)))
                  (cond ((> v1 v2) (return-from version-satisfies t))
                        ((< v1 v2) (return-from version-satisfies nil)))))
         v1 v2))
  t)

(defun load-asd (pathname)
  (let ((*package* (find-package '#:esdf/asdf-compat)))
    (load pathname)))

(ensure-load-hook "asd" #'load-asd)

(defun symbol-call (package name &rest args)
  (apply (find-symbol name (find-package package)) args))


;;; dummy classes
(defclass asdf-op    ()        ())
(defclass load-op    (asdf-op) ())
(defclass test-op    (asdf-op) ())
(defclass compile-op (asdf-op) ())

;;; dummy conditions
(define-condition missing-dependency-of-version (missing-dependency) ())


;;; main methods

(defmethod operate ((operation asdf-op) system
                    &key &allow-other-keys)
  ;; all not implemented operations return T
  (warn "calling operate from asdf")
  (case (class-name (class-of operation))
    ((load-op load-bundle-op monolithic-load-bundle-op)
     (operate :load system))
    (test-op
     (operate :test system))
    (otherwise T)))

(defmethod operate ((operation symbol) (system system)
                    &key &allow-other-keys)
  (ecase operation
    ((load-op :load-op) (operate :load system))))

(defgeneric perform (operation system)
  (:method (operation system)
    (funcall #'operate operation system)))


;;; ASDF-specific load-component
(defmethod esdf:load-component ((type (eql :file)) name &rest options)
  (let ((pathname (esdf:fix-pathname
                   (or (second (assoc 'pathname options))
                       (make-pathname :type "lisp" :defaults name))
                   *default-pathname-defaults*)))
    (load (compile-file pathname))))

(defmethod esdf:load-component ((type (eql :module)) name &rest options)
  (apply #'esdf:load-component 'esdf:module name options))


;;; function stubs

;;; ASDF looks for system changes or finds a new one in the registry.
;;; It doesn't fall into infinite recursion if the `asd' file calls
;;; `find-system' from its inside thanks to the cache.
;;;
;;; This model is conceptually broken imho and people use it with
;;; something different in mind (due to bad documentation). The
;;; following should work just fine for simple cases (and will break
;;; horribly for more sophisticated ones).
(defun find-system (system &optional error-p
                    &aux (name (coerce-name system)))
  (if (null error-p)
      (esdf:defined-system name nil)
      (esdf:defined-system name)))

(defun component-name (component)
  (when (typep component 'esdf:system)
    (esdf:system-name component)))

(defun component-pathname (component)
  (when (typep component 'esdf:system)
    (esdf:system-pathname component)))

(defun system-source-directory (system)
  (esdf:system-pathname system))

(defun initialize-source-registry (&optional parameter)
  (declare (ignore parameter)))


;;; lie a little

(pushnew :asdf *features*)
(pushnew :asdf2 *features*)
(pushnew :asdf3 *features*)
(pushnew :asdf3.1 *features*)
