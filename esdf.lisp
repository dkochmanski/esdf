
(defpackage #:esdf
  (:use #:cl)
  (:export #:oos #:operate #:load-component
           ;; system definition
           #:define-system #:load-esd
           ;; base system class
           #:system #:system-name #:system-pathname #:definition-pathname
           ;; base component-types
           #:file #:files #:module
           ;; system register manipulation
           #:ensure-system-hook #:remove-system-hook #:system-hooks
           #:defined-system #:register-system #:clear-system #:map-systems
           ;; conveniance functions
           #:loadedp #:load-system #:test-system
           ;; conditions
           #:missing #:missing-component #:missing-system
           #:depends #:unknown-dependency #:circular-dependency
           #:missing-dependency #:missing-feature
           ;; utilities
           #:fix-pathname #:coerce-name #:meta))
(in-package #:esdf)


;;; system base class

(defclass system ()
  ((name       :accessor system-name :initarg :name)
   (components :accessor components  :initarg :components :initform nil)
   (depends    :accessor depends     :initarg :depends    :initform nil)
   (meta       :accessor meta        :initarg :meta)
   (all-args   :accessor all-args    :initarg :all-args)
   (state :accessor state
          :initform :defined)
   (esd-pathname :accessor definition-pathname
                 :initarg :definition-pathname
                 :initform *load-truename*)
   (src-pathname :accessor system-pathname
                 :initarg :system-pathname
                 :initform *default-pathname-defaults*)))


;;; pathname-as-directory, pathname-directory-pathname
;;;
;;; These functions are copied from `cl-fad' (which is bundled with
;;; its licence file).
(defun component-present-p (value)
  "Helper function for DIRECTORY-PATHNAME-P which checks whether VALUE
   is neither NIL nor the keyword :UNSPECIFIC."
  (and value (not (eql value :unspecific))))

(defun directory-pathname-p (pathspec)
  "Returns NIL if PATHSPEC \(a pathname designator) does not designate
a directory, PATHSPEC otherwise.  It is irrelevant whether file or
directory designated by PATHSPEC does actually exist."
  (and
    (not (component-present-p (pathname-name pathspec)))
    (not (component-present-p (pathname-type pathspec)))
    pathspec))

(defun pathname-as-directory (pathspec)
  "Converts the non-wild pathname designator PATHSPEC to directory
form."
  (let ((pathname (pathname pathspec)))
    (when (wild-pathname-p pathname)
      (error "Can't reliably convert wild pathnames."))
    (cond ((not (directory-pathname-p pathspec))
           (make-pathname :directory (append (or (pathname-directory pathname)
                                                 (list :relative))
                                             (list (file-namestring pathname)))
                          :name nil
                          :type nil
                          :defaults pathname))
          (t pathname))))

(defun pathname-directory-pathname (pathname)
  "Returns a complete pathname representing the directory of
PATHNAME. If PATHNAME is already a directory pathname (name NIL, type
NIL) returns a pathname equal (as per pathname=) to it."
  (make-pathname :defaults pathname
                 :name nil :type nil))


;;; utilities

(defun coerce-name (system)
  (etypecase system
    (string system)
    (symbol (string-downcase system))
    (system (system-name system))))

(defgeneric loadedp (system)
  (:method ((system system))
    (eql (state system) :loaded))
  (:method ((system string))
    (warn "calling loadedp on string")))

(defun fix-pathname (path &rest defaults)
  (let ((result (reduce #'merge-pathnames defaults
                        :initial-value (or path "")
                        :key (lambda (p) (or p "")))))
    result))

(defun length=n (lst n)
  (if (or (= n 0) (null lst))
      (and (= n 0) (null lst))
      (length=n (cdr lst) (1- n))))

(defun assoc-value (alist key &key (test 'eql))
  (cdr (assoc key alist :test test)))


;;; conditions

(define-condition missing (error)
  ((requires    :initarg :requires    :accessor missing-requires)
   (required-by :initarg :required-by :accessor missing-required-by)))

(define-condition depends (error)
  ((dependency-type :initarg :type :accessor dependency-type)
   (dependency-body :initarg :body :accessor dependency-body)))

(define-condition missing-system    (missing) ()
  (:default-initargs :required-by nil))
(define-condition missing-component (missing) ())

(define-condition missing-dependency (missing depends) ())
(define-condition missing-feature (missing-dependency) ())
(define-condition unknown-dependency (depends)         ())
(define-condition definition-depends (missing)         ())

(define-condition circular-dependency (depends)
  ((dependency-a :accessor dependency-a)
   (dependency-b :accessor dependency-b)))

(defmethod print-object ((condition missing) stream)
  (if *print-escape*
      (call-next-method)
      (format stream "~s required by ~s is missing."
              (missing-requires condition)
              (missing-required-by condition))))

(defmethod print-object ((condition missing-system) stream)
  (if *print-escape*
      (call-next-method)
      (format stream "~s is missing."
              (missing-requires condition))))

(defmethod print-object ((condition depends) stream)
  (if *print-escape*
      (call-next-method)
      (format stream "~s dependency ~s failed."
              (dependency-type condition)
              (dependency-body condition))))


;;; hooks for systems being registered and cleaned
(defvar *system-hooks* nil)

(defun ensure-system-hook (register clean)
  (check-type register function)
  (check-type clean function)
  (pushnew (cons register clean)
           *system-hooks* :test #'equal))

(defun remove-system-hook (register clean)
  (check-type register function)
  (check-type clean function)
  (setf #1=*system-hooks*
        (remove (cons register clean) #1# :test #'equal)))

(defun system-hooks ()
  (copy-list *system-hooks*))


;;; systems access

(defvar *defined-systems* (make-hash-table :test 'equal)
  "Hash table embodying all defined systems")

(defun register-system (system)
  (check-type system system)
  (dolist (h *system-hooks*)
    (funcall (car h) system))

  ;; XXX: asdf stores system values as (timestamp . system)
  ;; entries. We need to investigate, if it is will be valuable for
  ;; us, or we should just emulate such behaviour in asdf-compat by
  ;; maintaining mirror hash-table of that form.

  ;; save the system definition
  (setf (gethash (system-name system) *defined-systems*) system))

(defun clear-system (system)
  (check-type system system)
  (dolist (h *system-hooks*)
    (funcall (cdr h) system))
  ;; remove the system definition
  (remhash (system-name system) *defined-systems*))

(defgeneric defined-system (name &optional default)

  (:method (name &optional (default nil default-supplied-p))
    (if default-supplied-p
        (defined-system (coerce-name name) default)
        (defined-system (coerce-name name))))

  (:method ((name string) &optional (default nil default-supplied-p))
    (or (gethash name *defined-systems* default)
        (unless default-supplied-p
          (error 'missing-system :requires name)))))

(defun map-systems (fn)
  (maphash (lambda (k v)
             (declare (ignore k))
             (funcall fn v))
           *defined-systems*))


;;; dependency resolve functions

(defun resolve-complex-dependency (resolve-function combinator argument)
  (flet ((check-node (argument)
           (funcall resolve-function combinator argument)))
    (ecase (car argument)
      ((or  :or ) (some  #'check-node (cdr argument)))
      ((and :and) (every #'check-node (cdr argument)))
      ((not :not) (if (length=n (cdr argument) 1)
                      (not (apply #'check-node (cdr argument)))
                      (error "Malformed ~s dependency (~s)"
                             combinator argument))))))

(defgeneric resolve-dependency-combinator (combinator &rest options)

  (:method (combinator &rest options)
    (cerror "Ignore unknown dependency."
            'unknown-dependency :type combinator :body options))

  (:method ((combinator cons) &rest options)
    (assert (null options))
    (assert (cdr combinator))  ; don't allow combinators without body.
    (apply #'resolve-dependency-combinator combinator))

  ;; If we have no options, then the dependency is simply an atom (so
  ;; it is a system).
  (:method ((combinator symbol) &rest options)
    (if (null options)
        (resolve-dependency-combinator 'system combinator)
        (call-next-method)))

  (:method ((combinator string) &rest options)
    (assert (null options))
    (resolve-dependency-combinator 'system combinator))

  (:method ((combinator (eql 'weakly)) &rest options)
    (handler-case (apply #'resolve-dependency-combinator options)
      (missing (c)
        (warn "Weak dependency ~s not fulfilled (missing ~s)"
              (car options) (missing-requires c)))))

  (:method ((combinator (eql 'feature)) &rest options)
    (let ((feature (car options)))
      (unless (member feature *features*)
        (error 'missing-feature :requires feature))))

  (:method ((combinator (eql 'module)) &rest options)
    (let ((module (car options)))
      (handler-case (require module)
        (error ()
          (load-system module :dont-reload t)))))

  #+ (or)
  (:method ((combinator (eql 'require)) &rest options)
    (let ((protocol (car options)))
      ;; we either look for explicitly named protocol and systems
      ;; implementing it
      (load-system
       (find-systems-imlementing-protocol (car options)))
      ;; later we try to load module (which names protocol implicitly)
      (handler-case (require module)
        (error ()
          ;; in the end we try to load a system (each system
          ;; implements its own implicit protocol)
          (load-system module)))))

  (:method ((combinator (eql 'system)) &rest options)
    (load-system (car options))))

(defgeneric resolve-component-combinator (combinator argument)

  (:method (combinator argument)
    (declare (ignore argument))
    (warn "Unknown component option ~s (ignoring)" combinator)
    T)

  (:method ((combinator (eql 'components)) argument)
    (declare (ignore combinator argument))
    T)

  (:method ((combinator (eql 'if-dependency)) argument)
    (if (atom argument)
        (loadedp argument)
        (case (car argument)
          ((and or not)
           (resolve-complex-dependency
            #'resolve-component-combinator combinator argument))
          (otherwise
           (warn "ignoring if-dependency ~s" argument)))))

  (:method ((combinator (eql 'if-feature)) argument)
    (if (atom argument)
        (member argument *features*)
        (resolve-complex-dependency
         #'resolve-component-combinator combinator argument))))

;;; Maps `function' over object dependencies. `function' is
;;; responsible for calling depends for recurrent traverse.
(defun map-dependencies (function system)
  (map () function (depends system)))


(defgeneric load-component (type name &rest options)

  (:method :around (type name &rest options)
    (when (every #'(lambda (opt)
                     (or (atom opt)
                         (resolve-component-combinator
                          (first opt) (second opt))))
                 options)
      (call-next-method)))

  (:method :around (type (name symbol) &rest options)
    (apply #'call-next-method type (coerce-name name) options))

  (:method (type name &rest options)
    (declare (ignore options))
    (warn "Skipping unknown component ~s ~s" type name))

  (:method ((type (eql 'file)) name &rest options)
    (let* ((pathname (second (assoc 'pathname options)))
           (pathname (fix-pathname
                      (or pathname name)
                      *default-pathname-defaults*)))
      (load (compile-file pathname))))

  (:method ((type (eql 'files)) file &rest options)
    (map () #'(lambda (f) (load (compile-file f)))
         (cons file options)))

  (:method ((type (eql 'module)) name &rest options)
    (let ((components (assoc-value options 'components))
          (pathname (car (assoc-value options 'pathname))))
      (dolist (component components)
        (let ((*default-pathname-defaults*
               (fix-pathname (pathname-as-directory (or pathname name))
                             *default-pathname-defaults*)))
          (apply #'load-component component))))))

(defgeneric operate (operation system
                     &key &allow-other-keys)

  ;; ensure, that we are operating on a system
  (:method (operation system
            &key &allow-other-keys)
    (operate operation (defined-system system)))

  ;; set up the necessary paths for loading
  (:method :around (operation (system system)
                    &key &allow-other-keys)
    (let ((*default-pathname-defaults* (system-pathname system)))
      (call-next-method)))

  ;; do the actual loading for the :load
  (:method ((operation (eql :load)) (system system)
            &key &allow-other-keys)
    (map-dependencies #'resolve-dependency-combinator system)
    (dolist (component (components system))
      (apply #'load-component component))
    (setf (state system) :loaded))

  ;; load only if not loaded yet (recursively)
  (:method ((operation (eql :require)) (system system)
            &key &allow-other-keys)
    (unless (loadedp system)
      (map-dependencies #'resolve-dependency-combinator system)
      (dolist (component (components system))
        (apply #'load-component component))
      (setf (state system) :loaded))
    T)

  ;; stub now
  (:method ((operation (eql :test)) (system system)
            &key &allow-other-keys)
    T))


;;; conveniance functions
(defun oos (&rest args)
  (apply #'operate args))

(defun load-system (system &rest args
                    &key dont-reload &allow-other-keys)
  (if dont-reload
      (apply #'operate :require system args)
      (apply #'operate :load system args)))

(defun test-system (system &rest args)
  (apply #'operate :test system args))


;;; define-protocol macro stub
(defmacro define-protocol (name base-protocols &body proto)
  (declare (ignore name base-protocols proto)))

;;; define-system macro
(defmacro define-system (name (&optional (class 'system)) &rest options)
  #+ (or)
  (cerror "Ignore. Symbols will be interned, no forward references."
          "DEFINE-SYSTEM isn't a lisp form, please load file with LOAD-ESD.")
  (mapc (lambda (system)
          (restart-case (unless (loadedp system)
                          (error 'definition-depends
                                 :requires system :required-by name))
            (try-load-system ()
              :report "Try to load definition dependency ad-hoc."
              (load-system system))))
        (assoc-value options 'definition-depends))

  `(let* ((name ,(coerce-name name))
          (path ,(fix-pathname (car (assoc-value options 'pathname))
                               (pathname-directory-pathname *load-truename*)
                               *default-pathname-defaults*))
          (system (make-instance
                   ',class
                   :name name
                   :definition-pathname ,*load-truename*
                   :system-pathname (pathname-as-directory (or path ""))
                   :components ',(assoc-value options 'components)
                   :depends ',(assoc-value options 'depends)
                   :meta ',(assoc-value options 'meta)
                   :all-args ',options)))
     (register-system system)))

;;; dummy macros to make indentation work
(defmacro file   (name &body body) (declare (ignore name body)))
(defmacro module (name &body body) (declare (ignore name body)))


;;; for now we set only package, but at some point we need to provide
;;; non-interning reader
(defun load-esd (pathname)
  (let ((*package* (find-package '#:esdf)))
    (load (make-pathname :type "esd" :defaults pathname))))

;;; provide a reader stub for the macro character #\e
(defun read-esd (stream char subchar)
  (declare (ignore char subchar))
  (let ((*package* (find-package '#:esdf)))
    (read stream)))


;;;; By now we should have basic functionality in place. For instance
;;;; we can load ESD systems given the system definition
;;;; pathname. That means, that we may load ESDF system definition
;;;; itself. Note, that we don't have find-system yet, so all esd
;;;; files should be loaded manually here.

(pushnew :esdf     *features*)
(pushnew :esdf-0.1 *features*)
(unless (member :asdf *features*)
  (pushnew :asdf-compat *features*))

#+:esdf (map-systems #'register-system)
#-:esdf (load-esd (merge-pathnames "esdf" *load-truename*))
#-:esdf (set-dispatch-macro-character #\# #\e #'read-esd)
