(defpackage #:esdf/logical-pathnames
  (:use #:cl)
  (:import-from #:esdf
                #:ensure-system-hook #:map-systems
                #:system-name #:system-pathname #:definition-pathname ))
(in-package #:esdf/logical-pathnames)


(defun coerce-tree (system)
  (substitute #\; '(#\/ #\.) system
              :test #'(lambda (old new)
                        (member new old))))


(setf (logical-pathname-translations "esdf")
      `(("esdf:"
         ,(cl-fad:pathname-directory-pathname *load-truename*))))

(defun ensure-ltr (elt)
  (setf #1=(logical-pathname-translations "esdf")
        (cons elt (remove (car elt) #1# :test #'equal :key #'car))))

(defun remove-ltr (elt)
  (setf #1=(logical-pathname-translations "esdf")
        (remove elt #1# :test #'equal :key #'car)))



(defun register-hook (system)
  ;; add full pathname to the system definition
  (let ((tree (coerce-tree (system-name system)))
        (defp (definition-pathname system))
        (sysp (merge-pathnames "**/*.*" (system-pathname system))))


    (unless (null defp)
      (ensure-ltr `(,(concatenate 'string "esdf:systems;" tree ".*.*") ,defp)))

    ;; translate all system-relative pathnames. We translate all
    ;; '/' and '.' in the system name into the directory
    ;; separator. If user has systems 'foo.bar' and 'foo/bar' it's
    ;; his own fault. components will be registered by name
    ;; (canonical) under "esdf:components;" to prevent collisions
    ;; between names and pathnames (if they differ).
    (unless (null sysp)
      (ensure-ltr `(,(concatenate 'string "esdf:systems;" tree ";**;*.*.*") ,sysp)))))

(defun clean-hook (system)
  (let ((tree (coerce-tree (system-name system))))
    (remove-ltr (concatenate 'string "esdf:systems;" tree ".*.*"))
    (remove-ltr (concatenate 'string "esdf:systems;" tree ";**;*.*.*"))))

(ensure-system-hook #'register-hook #'clean-hook)
(map-systems #'register-hook)
