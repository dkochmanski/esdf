(defpackage #:esdf/system-discovery
  (:use #:cl #:alexandria)
  (:export #:search-for-system-definition))
(in-package #:esdf/system-discovery)


(defvar *system-definition-search-functions* nil)

(defun ensure-discovery-function (function)
  "Adds new search function (if it doesn't exist)"
  (check-type function function)
  (pushnew function *system-definition-search-functions*))

(defun remove-discovery-function (function)
  "Removes search function"
  (check-type function function)
  (setf *system-definition-search-functions*
        (remove function *system-definition-search-functions*)))

(defun discovery-functions ()
  "Returns list of registered search functions"
  *system-definition-search-functions*)


(defvar *central-registry* nil)

(defun ensure-central-registry-entry (path)
  "Adds new path to search in the central registry"
  (check-type path (or string pathname))
  (pushnew path *central-registry* :test #'equal))

(defun remove-central-registry-entry (path)
  "Removes search path from the central registry"
  (check-type path (or string pathname))
  (setf #1=*central-registry* (remove path #1# :test #'equal)))

(defun central-registry-entries ()
  "Returns list of registered paths in central registry"
  *central-registry*)


(defvar *load-hooks* nil)

(defun ensure-load-hook (pathname-type function)
  "Ensures that `pathname-type' is associated with `function'"
  (check-type function function)
  (check-type pathname-type (or string symbol))
  (pushnew (cons (string-downcase pathname-type) function) *load-hooks* :test #'equal))

(defun remove-load-hook (pathname-type)
  "Remove `pathname-type' hook"
  (setf #1=*load-hooks*
        (remove (string-downcase pathname-type) #1# :test #'equal)))

(defun load-hooks ()
  "Return assoc list of load hooks"
  *load-hooks*)


;;; hack for slash-separated names (with a #\. bonus)
(defun primary-system-name (name)
  (let ((max (length name)))
    (subseq name 0 (min (or (position #\/ name) max)
                        (or (position #\. name) max)))))

;;; Find the system on the disk (recursively under directories in a
;;; variable `*central-registry')
(defun sysdef-central-registry-search (system)
  (map () #'(lambda (dir)
              (cl-fad:walk-directory
               dir #'(lambda (path)
                       (when (equalp
                              (pathname-name path)
                              (primary-system-name system))
                         (return-from sysdef-central-registry-search path)))
               :test #'(lambda (p) (member (pathname-type p)
                                           (mapcar #'car (load-hooks))
                                           :test #'equalp))))
       *central-registry*))


(defun search-for-system-definition (system)
  (let ((name (esdf:coerce-name system)))
    (some #'(lambda (f) (funcall f name))
          (discovery-functions))))

(defmethod esdf:defined-system :around
    ((name string) &optional (default nil default-supplied-p))
  (let ((system (call-next-method name :not-found)))
    (case system
      (:not-found
       (let ((path (search-for-system-definition name)))
         (typecase path
           ((or string pathname)
            (let ((load-hook (assoc-value (load-hooks) (pathname-type path) :test #'equal)))
              (if (null load-hook)
                  (error "Trying to load not supported file ~s" path)
                  (funcall load-hook path)))
            (call-next-method))
           (esdf:system
            (esdf:register-system system))
           (null (if default-supplied-p
                     default
                     (error 'esdf:missing-system :requires name))))))
      (otherwise system))))



(ensure-discovery-function #'sysdef-central-registry-search)
(ensure-central-registry-entry "esdf:systems;esdf;")
(ensure-load-hook "esd" #'esdf:load-esd)
