
(defsystem if-feature/1
  :description "system without feature dependencies"
  :components ((:file "if-feature")))

(defsystem if-feature/2
  :description "system with feature dependency"
  :components ((:file "if-feature" :if-feature :if-feature/f1)))

(defsystem if-feature/3
  :description "system with negated dependency"
  :components ((:file "if-feature" :if-feature (not :if-feature/f1))))

(defsystem if-feature/3b
  :description "system with negated dependency (2)"
  :components ((:file "if-feature" :if-feature (not :if-feature/f3))))

(defsystem if-feature/4
  :description "system with multiple dependencies"
  :components ((:file "if-feature" :if-feature (and :if-feature/f1 :if-feature/f2))))

(defsystem if-feature/4b
  :description "system with multiple dependencies (2)"
  :components ((:file "if-feature" :if-feature (or :if-feature/f3 :if-feature/f1))))

(defsystem if-feature/4c
  :description "system with multiple dependencies (3)"
  :components ((:file "if-feature" :if-feature (and :if-feature/f1 :if-feature/f3))))

(defsystem if-feature/4d
  :description "system with multiple dependencies (4)"
  :components ((:file "if-feature" :if-feature (and :if-feature/f1 (not :if-feature/f3)))))

(defsystem if-feature/5
  :description "system with keywords :and :or :not"
  :components ((:file "if-feature" :if-feature (:and :if-feature/f1 (:not :if-feature/f3)))))

(defsystem if-feature/5b
  :description "system with keywords :and :or :not (2)"
  :components ((:file "if-feature" :if-feature (:and :if-feature/f1
                                                     (:or :if-feature/f3
                                                          :if-feature/f2)))))

(defsystem if-feature/5c
  :description "system with keywords :and :or :not (3)"
  :components ((:file "if-feature" :if-feature (:and :if-feature/f1
                                                     (:or :if-feature/f3
                                                          (:not :if-feature/f2))))))

(defsystem if-feature/6
  :description "system with and/or/not empty clauses"
  :components ((:file "if-feature" :if-feature (and))))

(defsystem if-feature/6b
  :description "system with and/or/not empty clauses"
  :components ((:file "if-feature" :if-feature (or))))

(defsystem if-feature/6c
  :description "system with and/or/not empty clauses"
  :components ((:file "if-feature" :if-feature (not))))

(defsystem if-feature/7
  :description "malformed not dependency"
  :components ((:file "if-feature" :if-feature (not a b))))
