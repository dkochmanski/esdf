
(in-package #:esdf/test)

(defvar *if-feature/param*)

(test if-feature/asd
  ;; fix the initial state
  (pushnew :if-feature/f1 *features*)
  (pushnew :if-feature/f2 *features*)
  (setf *features* (remove :if-feature/f3 *features*))
  (setf *if-feature/param* 0)
  (asdf:load-asd "esdf:systems;esdf;test;component-definition-suite;if-feature.asd")

  (is (= *if-feature/param* 0))         ; dummy, always succeeds
  (flet ((assert-loaded (status system)
           (let ((expected (ecase status
                             (:fail *if-feature/param*)
                             (:pass (1+ *if-feature/param*)))))
               (load-system system)
               (is (= *if-feature/param* expected)
                   "~a: ~a" system
                   (alexandria:assoc-value
                    (esdf::meta (defined-system system))
                    'esdf::description)))))
    (assert-loaded :pass "if-feature/1")
    (assert-loaded :pass "if-feature/1")
    (assert-loaded :pass "if-feature/2")
    (assert-loaded :fail "if-feature/3")
    (assert-loaded :pass "if-feature/3b")
    (assert-loaded :pass "if-feature/4")
    (assert-loaded :pass "if-feature/4b")
    (assert-loaded :fail "if-feature/4c")
    (assert-loaded :pass "if-feature/4d")
    (assert-loaded :pass "if-feature/5")
    (assert-loaded :pass "if-feature/5b")
    (assert-loaded :fail "if-feature/5c")
    (assert-loaded :pass "if-feature/6")
    (assert-loaded :fail "if-feature/6b")
    (signals error (load-system "if-feature/6c"))
    (signals error (load-system "if-feature/7"))))
