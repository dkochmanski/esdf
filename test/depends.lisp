(in-package #:esdf/test)

#e
(progn
  (define-system null-system ())

  (define-system unknown-dependency ()
    (depends (booya "kashalaka")))

  (define-system string-dependency ()
    (depends "null-system"))

  (define-system symbol-dependency ()
    (depends null-system))

  (define-system system-string-dependency ()
    (depends (system "null-system")))

  (define-system system-symbol-dependency ()
    (depends (system null-system))))

;;; system `unknown-dependency' depends on option (booya "kashalaka")
;;; which doesn't have (or at least shouldn't) a handler and ought to
;;; signal an unknown-dependency condition.
(test unknown-dependency
  (signals esdf:unknown-dependency (load-system 'unknown-dependency)))

;;; variations of single system dependencies - implicit vs explicit
;;; and string vs symbol designators (on the same system)
(test system-dependencies
  (is-eql :loaded (load-system 'string-dependency))
  (is-eql :loaded (load-system 'symbol-dependency))
  (is-eql :loaded (load-system 'system-string-dependency))
  (is-eql :loaded (load-system 'system-symbol-dependency)))
