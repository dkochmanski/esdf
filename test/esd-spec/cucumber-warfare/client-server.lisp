(in-package #:cucumber-warfare)

(defmacro do-protocol (name &body proto)
  (declare (ignore name proto)))

;;; this is a protocol implementation stub. Protocol for defining
;;; protocols and implementing them is *not* developed yet.

(do-protocol client-server
  (defgeneric connect     (server) (:method     (server) T))
  (defgeneric send          (data) (:method       (data) T))
  (defgeneric recv    (max-length) (:method (max-length) T)))
