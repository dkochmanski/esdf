(defpackage #:cucumber-warfare
  (:use #:cl)
  (:import-from #:alexandria
                #:ensure-list
                #:ensure-gethash)
  (:export #:attack! #:defend!))
