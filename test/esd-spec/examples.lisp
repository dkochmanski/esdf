
(in-package #:esdf/test)

;;; expected fail for now
(test load-cucumber-warfare
  (finishes (load-esd "esdf:systems;esdf;test;esd-spec;cucumber-warfare.esd")))

(test if-dependency
  (finishes (load-esd "esdf:systems;esdf;test;esd-spec;hemlet.esd"))

  (let ((*features*
         (append (list :prayers :cars :illiteracy)
                 (set-difference *features* (list :cars :carts)))))
    (is-eql :loaded (load-system "hemlet"))
    (is-true  (loadedp "esdf:components;hamlet;church"))
    (is-true  (loadedp "esdf:components;hamlet;road"))
    (is-false (loadedp "esdf:components;hamlet;cheap-bar"))
    (is-false (loadedp "esdf:components;hamlet;library"))
    (is-true  (loadedp "esdf:components;hamlet;community-centre")))
  
  (let ((*features*
         (append (list :prayers :illiteracy)
                 (set-difference *features* (list :cars :carts)))))
    (signals warning (load-system "hemlet"))
    (is-true  (loadedp "esdf:components;hamlet;church"))
    (is-false (loadedp "esdf:components;hamlet;road"))
    (is-false (loadedp "esdf:components;hamlet;cheap-bar"))
    (is-false (loadedp "esdf:components;hamlet;library"))
    (is-true  (loadedp "esdf:components;hamlet;community-centre")))

  (let ((*features*
         (append (list :prayers :illiteracy)
                 (set-difference *features* (list :cars :carts)))))
    (signals missing-component (load-system "hemlet"))
    (is-true  (loadedp "esdf:components;hamlet;church"))
    (is-false (loadedp "esdf:components;hamlet;road"))
    (is-false (loadedp "esdf:components;hamlet;cheap-bar"))
    (is-false (loadedp "esdf:components;hamlet;library"))
    (is-true  (loadedp "esdf:components;hamlet;community-centre"))))
