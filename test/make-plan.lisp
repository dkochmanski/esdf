
(in-package #:esdf/test)

#e(progn
    (define-system root-system1 ()
      (depends middle-system end-system))
    (define-system root-system2 ()
      (depends middle-system absent-system))
    (define-system root-system3 ()
      (depends middle-system circular-one))
    (define-system middle-system ()
      (depends end-system))
    (define-system end-system ()
      (meta (description "system without dependencies")))
    (define-system circular-one ()
      (depends circular-two))
    (define-system circular-two ()
      (depends circular-one)))

(test make-plan
      (is (make-plan 'root-system1)
          '((:load end-system)
            (:load middle-system)
            (:load root-system1)))
      (signals 'missing-dependency  (make-plan 'root-system2))
      (signals 'circular-dependency (make-plan 'root-system3)))
